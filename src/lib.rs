mod fuzzysearch;
use std::path::Path;
use std::fs;
use std::io::{self, Write};
use gitignore::File;
use crossterm::{
    event::{self, Event, KeyCode, KeyEvent},
    terminal::{self, ClearType},
    cursor,
    style::{self, Print, Color},
    queue,
};

pub enum SelectionResult {
    Selection(String),
    Canceled,
}

pub fn search_cli(search_dirs: &Vec<&Path>) -> SelectionResult {
    let dir_results = search_dirs.iter().map(|&d| read_dir_recursive(&d))
        .collect::<Result<Vec<_>, _>>().unwrap();
    let files: Vec<String> = dir_results.iter()
        .flatten()
        .map(|s| s.clone())
        .collect();
    let (_, rows) = terminal::size().unwrap();
    let mut stderr = io::stderr();
    queue!(stderr, terminal::EnterAlternateScreen, cursor::Hide).unwrap();
    terminal::enable_raw_mode().unwrap();
    let mut search = String::new();
    let mut highlight = 0;
    let mut skip = 0;
    let mut max_results = rows as usize-1;
    let result;
    loop {
        let matches = fuzzysearch::search(&files, &search);
        queue!(stderr,
            terminal::Clear(ClearType::All),
            cursor::MoveTo(0, 0),
            Print(format!("> {}", search)),
            cursor::MoveToNextLine(1),
        ).unwrap();
        for (i, o) in matches.iter().enumerate().skip(skip).take(max_results) {
            let mut line = style::style(o);
            if i==highlight {
                line = line.with(Color::Black).on(Color::White);
            }
            queue!(stderr,
                Print(line),
                cursor::MoveToNextLine(1),
            ).unwrap();
        }
        match event::read().unwrap() {
            Event::Key(KeyEvent {code: c, ..}) => match c {
                KeyCode::Up => {
                    highlight = (highlight as i32 -1).max(0) as usize;
                    if highlight<skip {
                        skip = highlight;
                    }
                },
                KeyCode::Down => {
                    highlight = (highlight+1).min(matches.len()-1);
                    if skip+max_results <= highlight {
                        skip = highlight-max_results+1;
                    }
                },
                KeyCode::Enter => {
                    match matches.get(highlight) {
                        Some(v) => {
                            result = SelectionResult::Selection(String::from(*v));
                            break;
                        },
                        _ => (),
                    };
                },
                KeyCode::Esc => {
                    result = SelectionResult::Canceled;
                    break;
                },
                KeyCode::Char(c) => {
                    highlight = 0;
                    skip = 0;
                    search.push(c);
                },
                KeyCode::Backspace => {
                    highlight = 0;
                    skip = 0;
                    match search.pop() {_ => ()};
                },
                _ => (),
            },
            Event::Resize(_, height) => max_results = height as usize - 1,
            _ => (),
        };
        highlight = highlight.min(skip+max_results-1);
    }
    terminal::disable_raw_mode().unwrap();
    queue!(stderr, terminal::LeaveAlternateScreen, cursor::Show).unwrap();
    result
}

fn read_dir_recursive(dir: &Path) -> io::Result<Vec<String>> {
    let gitignore = dir.join(".gitignore");
    let ignores = File::new(&gitignore).ok();
    let mut files = vec![];
    fn process(root: &Path, dir: &Path, ignores: &Option<File>,
       files: &mut Vec<String>) -> io::Result<()>
    {
        for entry in fs::read_dir(dir)? {
            let path = entry?.path();
            let fname = path.file_name().unwrap();
            if fname == ".git" || fname == ".gitignore" {
                continue;
            }
            let local_path = path.strip_prefix(root).unwrap();
            if let Some(ignores) = ignores {
                if ignores.is_excluded(&path).unwrap() {
                    continue;
                }
            }
            if path.is_dir() {
                process(root, &path, ignores, files)?;
            } else if let Some(s) = local_path.to_str() {
                files.push(String::from(s));
            }
        }
        Ok(())
    };
    process(dir, dir, &ignores, &mut files)?;
    Ok(files)
}
