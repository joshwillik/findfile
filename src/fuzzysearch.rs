pub fn search<'a>(options: &'a Vec<String>, search: &String)
    -> Vec<&'a String>
{
    options.iter().filter(|opt| {
        let mut search_chars = search.chars();
        let mut needle = search_chars.next();
        for c in opt.chars() {
            if let Some(n) = needle {
                if n==c || n==c.to_lowercase().next().unwrap() {
                    needle = search_chars.next();
                }
            }
            if let None = needle {
                return true;
            }
        }
        needle.is_none()
    }).collect()
}

#[cfg(test)]
mod tests {
    use super::*;
    fn t(list: Vec<&str>, q: &str, exp: Vec<&str>) {
        let list = list.iter().map(|s| String::from(*s)).collect();
        let _exp = exp.iter()
            .map(|s| String::from(*s)).collect::<Vec<String>>();
        let exp: Vec<&String> = _exp.iter().collect();
        assert_eq!(search(&list, &String::from(q)), exp);
    }
    #[test]
    fn matches_right_order() {
        t(vec![
          "foobar",
          "raboof",
        ], "foo", vec![
          "foobar",
        ]);
        t(vec![
          "src/public/js/TodoApp.jsx",
        ], "pubtodo", vec![
          "src/public/js/TodoApp.jsx",
        ]);
    }
    #[test]
    fn all_files_lower_search() {
        t(vec![
          "Foo",
          "bar",
          "testing",
          "CorporatePeoplePackage",
          "CorporatePeoplePicker",
          "corporatepeoplepackage",
        ], "cpp", vec![
          "CorporatePeoplePackage",
          "CorporatePeoplePicker",
          "corporatepeoplepackage",
        ]);
    }
    #[test]
    fn upper_files_upper_search() {
        t(vec![
          "Foo",
          "bar",
          "testing",
          "CorporatePeoplePackage",
          "CorporatePeoplePicker",
          "corporatepeoplepackage",
        ], "CPP", vec![
          "CorporatePeoplePackage",
          "CorporatePeoplePicker",
        ]);
    }
}
