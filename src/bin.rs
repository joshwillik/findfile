use std::process;
use std::env;
use findfile::{self, SelectionResult};
const CODE_CANCEL: i32 = -2;

fn main() {
    let current = env::current_dir().unwrap();
    match findfile::search_cli(&vec![&current]) {
        SelectionResult::Selection(s) => println!("{}", s),
        SelectionResult::Canceled => process::exit(CODE_CANCEL),
    }
}
